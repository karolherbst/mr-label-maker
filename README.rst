Mr Label Maker
==============

Script for automatic labeling of issues and merge requests for projects
hosted on gitlab.freedesktop.org.

Dependencies:

- python3
- pip
- pip modules from requirements.txt

Run instructions:

.. code-block:: sh

  $ pip3 install -r requirements.txt
  $ ./mr_label_maker.py -p mesa -m -i -t "$TOKEN" -d
